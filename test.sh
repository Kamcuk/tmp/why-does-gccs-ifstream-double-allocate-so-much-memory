#!/bin/bash
set -euo pipefail

if [[ "${1:-}" = "--install" ]]; then
	if [[ -e "/etc/arch-release" ]]; then
		pacman -Sy --needed --noconfirm clang gcc base base-devel valgrind libc++
	fi
fi
cd "$(dirname "$(readlink -f "$0")")"


f_in() {
	local cc=$1 file=$2 name=$3
	echo "------- SECTION START $* ----------"
	# yes - word splitted
	$cc -v "$file" -o _build/"$name"
	valgrind --tool=memcheck --leak-check=yes _build/"$name"
	valgrind --tool=massif --threshold=0.0 --massif-out-file=_build/"$name".massif _build/"$name"
	ms_print --threshold=0.0 _build/"$name".massif
	echo "------- SECTION END $* ----------"
}

f() {
	f_in g++ "$1".cpp gcc_"$1"
	f_in clang++ "$1".cpp clang_"$1"
	f_in "clang++ -std=c++14 -stdlib=libc++ -I/usr/include/c++/v1" "$1".cpp clang_llvm_"$1"
}
set -x
env
mkdir -p _build
f stackoverflow
f stackoverflow_scanf

